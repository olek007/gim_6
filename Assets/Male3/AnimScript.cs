﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimScript : MonoBehaviour {

    Animation animation;
	// Use this for initialization
	void Start () {
        animation = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
        bool keyDown = Input.GetKeyDown(KeyCode.Space);
        if(keyDown)
        {
            animation.Play();
        }
	}
}
